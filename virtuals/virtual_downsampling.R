# 9 by 9 native resolution
library(raster)
#library(rasterVis)
library(ggplot2)
#install.packages("ggplot2")
#install.packages("ggpubr")
library(ggpubr)
#install.packages("tidyverse")
require(tidyverse)
#install.packages("matrixStats")
#library(matrixStats)
#install.packages("tolerance")
#library(tolerance)
source("./plot_helpers.R")
source("../novelDownsampling/best_downsampling_3.1.R")

# syntethic dataset generation ####
set.seed(2) # 2 is what has been used for the paper
soil <- round(runif(40, min=1,max = 30))
trees <- round(runif(41, min=61,max = 90))
grassfree <- raster(matrix(sample(c(soil,trees)),nrow=9))
extent(grassfree) <- extent(0,9,0,9)
thresholds <- c(0,30,1,30,60,2,60,90,3)
plotSpectral(grassfree)$raster

# Synthetic practical examples ####
# just for following patterns
grassfree_native <- grassfree
# classified synthetic dataset
grassfree_classified <- reclassify(grassfree_native,thresholds)

# plot of the classified synthetic dataset
fig_grassfree_native <- plotFigure(grassfree,grassfree_native,grassfree_classified)
# ggexport(fig_grassfree_native,
#          filename = "../figures/raster/fig_grassfree_native.png",
#          width=320, height = 256
# )

# todo: ask stackoverflow or someone about this
# it's weird that the next line works as intended, so as the one below it
#plotClassified(native_classified) + scale_x_discrete(limits=0:3*3)+scale_y_discrete(limits=0:3*3) + theme_linedraw()+theme(panel.background=element_rect(fill=NA),panel.ontop = TRUE)
#plotClassified(native_classified) + scale_x_continuous(minor_breaks = NULL, breaks=0:3*3)+scale_y_continuous(minor_breaks = NULL,breaks=0:3*3) + theme_linedraw()+theme(panel.background=element_rect(fill=NA),panel.ontop = TRUE)

# average downsampled, classified, and plotted
grassfree_mean <- aggregate(grassfree,3,mean)
grassfree_mean_classified <- reclassify(grassfree_mean,thresholds)
fig_grassfree_mean <- plotFigure(grassfree,grassfree_mean,grassfree_mean_classified)
fig_grassfree_mean
# ggexport(fig_grassfree_mean,
#          filename = "../figures/raster/fig_grassfree_mean.png",
#          width=320, height = 256
# )

# function that selects the central pixel, but only for 3 by 3 patches
selectCentral <- function(x,na.rm){
  return(x[5])
}
# central downsampled, classified, and plotted
grassfree_central <- aggregate(grassfree,3,selectCentral)
grassfree_central_classified <- reclassify(grassfree_central,thresholds)
fig_grassfree_central <- plotFigure(grassfree,grassfree_central,grassfree_central_classified)
fig_grassfree_central
# ggexport(fig_grassfree_central,
#          filename = "../figures/raster/fig_grassfree_central.png",
#          width=320, height = 256
# )

# function that selects 1 random pixel
selectRandom <- function(x,na.rm){
  sample_no <- sample(x,1)
  return(sample_no)
}
# random downsampled, classified, and plotted
grassfree_random <- aggregate(grassfree,3,selectRandom)
grassfree_random_classified <- reclassify(grassfree_random,thresholds)
fig_grassfree_random <- plotFigure(grassfree,grassfree_random,grassfree_random_classified)
fig_grassfree_random
# ggexport(fig_grassfree_random,
#          filename = "../figures/raster/fig_grassfree_random.png",
#          width=320, height = 256
# )

# histogram downsampled, classified, and plotted
grassfree_novel <- bestDownsampling(grassfree,3)
grassfree_novel_classified <- reclassify(grassfree_novel,thresholds)
grassfree_novel_plot <- plotClassified(grassfree_novel_classified,3,"Grassfree by novel")
grassfree_novel_plot$raster

# final plotting of figure 1
legends <- ggarrange(spectral_plot$legend,averaged_plot$legend)

indexedAndLegends <- ggarrange(spectral_plot$raster,legends,align="hv")
classifications <- ggarrange(
          native_plot$raster,native_plot$pie,
          central_plot$raster,central_plot$pie,
          averaged_plot$raster,averaged_plot$pie,
          random_plot$raster,random_plot$pie,
          ncol=4,nrow = 2,
          align = "hv"
          )
ggarrange(indexedAndLegends,classifications,ncol=1,nrow=2,align = "v",heights = c(1,2,2), widths = c(1,1))

spectral_plot$raster

### Central pixel downsampling issues 
# 9 by 9 raster that shows aliasing of trees on bare soil
orchard_base <- raster(matrix(runif(100, min = 10, max = 30), nrow = 10))
extent(orchard_base) <- extent(0, 10, 0, 10)
orchard_base[((1:3)*3),((1:3)*3)] <- runif(9, min=71,max = 90)

# regular classification
# classified synthetic dataset
orchard_match <- crop(orchard_base,c(1,10,0,9))
orchard_match_native <- orchard_match
orchard_match_classified <- reclassify(orchard_match,thresholds)
fig_orchard_match_native <- plotFigure(orchard_match,orchard_match_native,orchard_match_classified)
fig_orchard_match_native
ggexport(fig_orchard_match_native,
         filename = "../figures/raster/fig_orchard_match_native.png",
         width=320, height = 256
)

# central downsampled, classified, and plotted, with aliasing on signal
orchard_match_central <- aggregate(orchard_match,3,selectCentral)
orchard_match_central_classified <- reclassify(orchard_match_central,thresholds)
fig_orchard_match_central <- plotFigure(orchard_match,orchard_match_central,orchard_match_central_classified)
fig_orchard_match_central
ggexport(fig_orchard_match_central,
         filename = "fig_orchard_match_central.png",
         width=320, height = 256
)

# central downsampled, classified, and plotted, with aliasing on offset
orchard_offset <- crop(orchard_base,c(0,9,0,9))
orchard_offset_classified <- reclassify(orchard_offset,thresholds)

orchard_offset_central <- aggregate(orchard_offset,3,selectCentral)
orchard_offset_central_classified <- reclassify(orchard_offset_central,thresholds)
fig_orchard_offset_central <- plotFigure(orchard_offset,orchard_offset_central,orchard_offset_central_classified)
fig_orchard_offset_central
ggexport(fig_orchard_offset_central,
         filename = "../figures/raster/fig_orchard_offset_central.png",
         width=320, height = 256
)

# orchard match, random downsampled, classified, and plotted
orchard_match_random <- aggregate(orchard_match,3,selectRandom)
orchard_match_random_classified <- reclassify(orchard_match_random,thresholds)
fig_orchard_match_random <- plotFigure(orchard_match,orchard_match_random,orchard_match_random_classified)
fig_orchard_match_random
ggexport(fig_orchard_match_random,
         filename = "fig_orchard_match_random.png",
         width=320, height = 256
)

# orchard match, mean downsampled, classified, and plotted
orchard_match_mean <- aggregate(orchard_match,3,mean)
orchard_match_mean_classified <- reclassify(orchard_match_mean,thresholds)
fig_orchard_match_mean <- plotFigure(orchard_match,orchard_match_mean,orchard_match_mean_classified)
fig_orchard_match_mean
ggexport(fig_orchard_match_mean,
         filename = "../figures/raster/fig_orchard_match_mean.png",
         width=320, height = 256
)

# random
# Random sampling can give different results depending on the heterogeneity of the data.
# If high heterogeneity is expected in the dataset, especially inside an areal unit to be aggregated,
# then random sampling will not yield consistent results.
# In other words, repeating the same random downsampling followed by classification,
# with a different seed, will yield a high variance in results.
# A high areal unit is likely to increase heterogeneity.
# Therefore, random sampling should be approached carefully when downsampling with a high factor.
# Heterogeneity is closely linked to entropy, which is linked to compressibility.
# An image can be easily checked for compressibility

# A quick rule of thumb is that if the original dataset is not compressible,
# random downsampling is more likely to yield unreliable results.

# We will observe random downsampling variation, on:
# 1. patterned data, i.e. native_random_alias
# 2. random data, i.e. native
# 3. homogenous data, new native_homogeneous
# random downsampled, classified, and plotted
random_repeat <- function(i,native){
  aggregated <- aggregate(native,3,selectRandom)
  classified <- reclassify(aggregated,thresholds)
  classified_counts <- table(classified[])
  classified_percentages <- as.data.frame(100*classified_counts/sum(classified_counts))
  classes_template <- data.frame(Var1=factor(c(1,2,3)))
  result <- merge(classes_template,classified_percentages,by="Var1",all=T,sort=TRUE)
  names(result) <- c("Class",paste0("Freq",i))
  return(result)
}

## 1. patterned data, i.e. the preveious native_alias_signal
random_alias <- reduce(lapply(1:1000,random_repeat,native_alias_signal), full_join, by="Class") %>% replace(., is.na(.), 0)
random_alias$sd <- apply(random_alias[,startsWith(colnames(random_alias),"Freq")],1,sd)
random_alias$mean <- apply(random_alias[,startsWith(colnames(random_alias),"Freq")],1,mean)
print(paste("Variability of random downsampling on original data",random_alias$sd))
sum(abs(MoranLocal(native_alias_signal)[]))
#sum(raster.entropy(native_alias_signal)[],na.rm = TRUE)
Moran(native_alias_signal)

# ?tolerance
# plot(random_alias)
# hist(unlist(random_alias[1,-1]))
# hist(unlist(random_alias[3,-1]))
# shapiro.test(unlist(random_alias[3,-1]))
# plot(unlist(random_results[2,-1]))
# is.numeric(unlist(random_results[1,-1]))
# random_alias_plot <- plotClassified(random_alias_classified,3,"random")
# random_alias_plot$raster
# print(random_alias_plot$pie)

## 2. random data, i.e. the original native
random_original <- reduce(lapply(1:1000,random_repeat,native), full_join, by="Class") %>% replace(., is.na(.), 0)
random_original$sd <- apply(random_original[,startsWith(colnames(random_original),"Freq")],1,sd)
random_original$mean <- apply(random_original[,startsWith(colnames(random_original),"Freq")],1,mean)
print(paste("Variability of random downsampling on original data",random_original$sd))
Moran(native)
sum(abs(MoranLocal(native)[]))
sum(raster.entropy(native)[],na.rm = TRUE)

## 3. homogeneous data, random sampling
native_homogeneous <- raster(matrix(runif(81, min=30,max = 60),nrow=9))
extent(native_homogeneous) <- extent(0,9,0,9)

trees_selection <- matrix(FALSE,nrow=9,ncol=9)
trees_selection
trees_selection[row(trees_selection)-2>col(trees_selection)] <- TRUE
trees_selection <- lower.tri(trees_selection,diag = TRUE)

soil_selection <- matrix(FALSE,nrow=9,ncol=9)
soil_selection
soil_selection[row(soil_selection)+2<col(soil_selection)] <- TRUE
soil_selection <- upper.tri(soil_selection)

native_homogeneous[trees_selection] <- runif(sum(trees_selection), min=60,max = 90)
native_homogeneous[soil_selection] <- runif(sum(soil_selection), min=00,max = 30)
plot(native_homogeneous)

Moran(native_homogeneous)
#sum(abs(MoranLocal(native_homogeneous)[]))
#sum(raster.entropy(native_homogeneous)[],na.rm = TRUE)

border_native_plot <- plotSpectral(native_homogeneous)
border_native_classified <- reclassify(native_homogeneous,thresholds)
border_native_classified_plot <- plotClassified(border_native_classified,1,"")
random_border <- aggregate(native_homogeneous,3,selectRandom)
random_border_plot <- plotSpectral(random_border,3)
random_border_classified <- reclassify(random_border,thresholds)
random_border_classified_plot <- plotClassified(random_border_classified,3,"")
random_border_classified_plot$raster
random_border_plot$pie

ggexport(ggarrange(
  border_native_plot$raster,
  border_native_plot$legend,
  border_native_classified_plot$raster,
  border_native_classified_plot$pie,
  border_native_classified_plot$legend,
  random_border_plot$raster,
  random_border_plot$legend,
  random_border_classified_plot$raster,
  random_border_classified_plot$pie,
  random_border_classified_plot$legend,
  ncol = 5,nrow=2,align="hv"),
  filename = "fig_random_border.png",
  width=960, height = 480
)


random_homogeneous <- reduce(lapply(1:1000,random_repeat,native_homogeneous), full_join, by="Class") %>% replace(., is.na(.), 0)
random_homogeneous$sd <- apply(random_homogeneous[,startsWith(colnames(random_homogeneous),"Freq")],1,sd)
random_homogeneous$mean <- apply(random_homogeneous[,startsWith(colnames(random_homogeneous),"Freq")],1,mean)
print(paste("Variability of random downsampling on homogeneous data",random_homogeneous$sd))


random_sds <- c(random_original$sd[1],
random_alias$sd[1],
random_homogeneous$sd[1])
random_moransi <- c(Moran(native),
Moran(native_alias_signal),
Moran(native_homogeneous))
cor(random_sds,random_moransi)
cor(random_sds,abs(random_moransi))

# Proportional downsampling
# proportional downsampling calculates histogram class frequency,
# using equal sized bins, and slightly more bins than the expected final classes.
# If the final number of classes is unknown, the default provided by the software is likely ok.
# The goal is to select representative values such as the histogram class frequency stays the same after aggregation.
# The above goal is based on the assumption that closer values are more likely to yield similar results in the final classification
# In a sense this method is stratifying the values based on both their values,
# and their position, and not only on the later as in all the other methods, while giving priority to the former.
# There are multiple ways to achieve this goal, below is one possible method

## Proportional downsampling  on: ####
source("./best_downsampling_3.R")
# 1. grass-free
# 2. orchard
# 2.1 match
# 2.2 offset
# 3. homogeneous data
# 4. real dataset (already run)
# 5. bfast (on the latest frame, use which)
# 5.1 bfast-central
# 5.2 bfast-random
# 5.3 bfast-average
# 5.4.1 bfast-proportional
# 5.4.2 bfast-proportional multiple factors

# 1. original native, best downsampled and classified ####
grassfree_novel <- bestDownsampling(grassfree,3)
grassfree_novel_classified <- reclassify(grassfree_novel,thresholds)
fig_grassfree_novel <- plotFigure(grassfree,grassfree_novel,grassfree_novel_classified)
fig_grassfree_novel
ggexport(fig_grassfree_novel,
         filename = "fig_grassfree_novel.png",
         width=320, height = 256
)

best_original_downsampled <- bestDownsampling(native_original,3)
best_original_plot <- plotSpectral(best_original_downsampled,3)
best_original_classified <- reclassify(best_original_downsampled,thresholds)
best_original_classified_plot <- plotClassified(best_original_classified,3)
best_original_classified_plot$raster
best_original_classified_plot$pie
best_original_arranged_plot <- ggarrange(
  best_original_plot$raster,
  best_original_plot$legend,
  best_original_classified_plot$raster,
  best_original_classified_plot$pie,
  best_original_classified_plot$legend,
  ncol = 5,nrow=1,align="hv")

# 2.1 patterned-alias, best downsampled and classified ####
orchard_novel <- bestDownsampling(orchard_match,3)
orchard_novel_classified <- reclassify(orchard_novel,thresholds)
fig_orchard_novel <- plotFigure(orchard_match,orchard_novel,orchard_novel_classified)
fig_orchard_novel
ggexport(fig_orchard_novel,
         filename = "fig_orchard_novel.png",
         width=320, height = 256
)

native_alias_signal_2 <- native_alias_signal
best_alias_downsampled <- bestDownsampling(native_alias_signal_2,3,breaks=5)
best_alias_plot <- plotSpectral(best_alias_downsampled,3)
plot(native_alias_signal_2)
plot(best_alias_downsampled)
best_alias_classified <- reclassify(best_alias_downsampled,thresholds)

# plot of the classified synthetic dataset
best_alias_classified_plot <- plotClassified(best_alias_classified,downsamplingFactor = 3,caption = "")
best_alias_classified_plot$raster
best_alias_classified_plot$pie
best_alias_arranged_plot <- ggarrange(
  best_alias_plot$raster,
  best_alias_plot$legend,
  best_alias_classified_plot$raster,
  best_alias_classified_plot$pie,
  best_alias_classified_plot$legend,
  ncol = 5,nrow=1,align="hv")

# 2.2 patterned-offset, best downsampled and classified ####
# Note: same as for patterned-alias
native_alias_offset_2 <- native_alias_offset
#native_alias_offset_2[6] <- 83.1
best_alias_offset_downsampled <- bestDownsampling(native_alias_offset_2,3,breaks=2)
best_alias_offset_plot <- plotSpectral(best_alias_offset_downsampled,3)
best_alias_offset_classified <- reclassify(best_alias_offset_downsampled,thresholds)
# plot of the classified synthetic dataset
best_alias_offset_classified_plot <- plotClassified(best_alias_offset_classified,downsamplingFactor = 3,caption = "")
best_alias_offset_classified_plot$raster
best_alias_offset_classified_plot$pie

best_alias_offset_arranged_plot <- ggarrange(
  best_alias_offset_plot$raster,
  best_alias_offset_plot$legend,
  best_alias_offset_classified_plot$raster,
  best_alias_offset_classified_plot$pie,
  best_alias_offset_classified_plot$legend,
  ncol = 5,nrow=1,align="hv")

# 3. homogeneous data, best downsampled and classified ####
plot(native_homogeneous)
best_homogeneous_downsampled <- bestDownsampling(native_homogeneous,3,breaks=2)
best_homogeneous_plot <- plotSpectral(best_homogeneous_downsampled,3)
best_homogeneous_classified <- reclassify(best_homogeneous_downsampled,thresholds)
best_homogeneous_classified_plot <- plotClassified(best_homogeneous_classified,downsamplingFactor = 3, caption = "")
best_homogeneous_classified_plot$raster
best_homogeneous_classified_plot$pie

best_homogeneous_arranged_plot <- ggarrange(
  best_homogeneous_plot$raster,
  best_homogeneous_plot$legend,
  best_homogeneous_classified_plot$raster,
  best_homogeneous_classified_plot$pie,
  best_homogeneous_classified_plot$legend,
  ncol = 5,nrow=1,align="hv")

# Comparison figure ####
best_arranged <- ggarrange(
  best_original_arranged_plot,
  best_alias_arranged_plot,
  best_alias_offset_arranged_plot,
  best_homogeneous_arranged_plot,
  ncol = 1,nrow=4,align="hv"
)
ggexport(best_arranged,
         filename = "fig_best_synthetic.png",
         width=960, height = 960
         )

# 4. real dataset, see above

# 5. bfast
# bfast native - the dataset 
bfast_native_input <- brick("C:\\bfast-test\\NDMI_2010-01-01_2020-06-01.tif")
bfast_native_result <- raster("C:/bfast-test/NDMI_BFAST_native.tif")
plot(bfast_native_result, main = "native")
plot(bfast_native_result)

# 5.1 bfast central downsampled
bfast_central_downsampled <- aggregate(bfast_native_input,3, selectCentral)
writeRaster(bfast_central_downsampled,"C:\\bfast-test\\NDMI_2010-01-01_2020-06-01_central.tif")
bfast_central_result <- raster("C:/bfast-test/NDMI_BFAST_central.tif")
plot(bfast_central_result, main = "central")
# 5.2 bfast random downsampled
bfast_random_downsampled <- aggregate(bfast_native_input, 3, selectRandom)
writeRaster(bfast_random_downsampled,"C:/bfast-test/NDMI_2010-01-01_2020-06-01_random.tif")
bfast_random_result <- raster("C:/bfast-test/NDMI_BFAST_random.tif")
plot(bfast_random_result, main = "random")
# 5.3 bfast average downsample
bfast_mean_downsampled <- aggregate(bfast_native_input, 3, mean)
writeRaster(bfast_mean_downsampled,"C:/bfast-test/NDMI_2010-01-01_2020-06-01_mean.tif")
bfast_mean_result <- raster("C:/bfast-test/NDMI_BFAST_mean.tif")
plot(bfast_mean_result, main = "mean")
# 5.4 bfast best downsample
bfast_final_frame <- bfast_native_input[[nlayers(bfast_native_input)]]
plot(bfast_final_frame)
bfast_best_downsampled <- bestDownsampling(bfast_final_frame,3)
bfast_best_upsampled <- disaggregate(bfast_best_downsampled,3)
bfast_best_possible_positions <- bfast_best_upsampled==crop(bfast_final_frame,bfast_best_upsampled)
bfast_native_filtered <- crop(bfast_native_input,bfast_best_upsampled)
plot(bfast_native_input[[120]])
bfast_native_filtered[!bfast_best_possible_positions] <- NA
plot(bfast_native_filtered[[120]])
plot(bfast_best_possible_positions)
selectFirstValue <- function(x,na.rm){
  if(!all(is.na(x))){
    x <- x[!is.na(x)]
    result <- x[1]
  }
  else{
    result <- NA
  }
  return(result)
}
bfast_best_downsampled_stack <- aggregate(bfast_native_filtered, 3, selectFirstValue)
writeRaster(bfast_best_downsampled_stack,"C:/bfast-test/NDMI_2010-01-01_2020-06-01_best.tif")
bfast_best_result <- raster("C:/bfast-test/NDMI_BFAST_best.tif")
plot(bfast_best_result,main = "best3")

# 5.5 bfast best downsample x5
bfast_best5_downsampled <- bestDownsampling(bfast_final_frame, 5)
bfast_best5_upsampled <- disaggregate(bfast_best5_downsampled, 5)
bfast_best5_possible_positions <- bfast_best5_upsampled==crop(bfast_final_frame,bfast_best5_upsampled)
bfast_native5_filtered <- crop(bfast_native_input,bfast_best5_upsampled)
bfast_native5_filtered[!bfast_best5_possible_positions] <- NA
bfast_best5_downsampled_stack <- aggregate(bfast_native5_filtered,5,selectFirstValue)
writeRaster(bfast_best5_downsampled_stack,"C:/bfast-test/NDMI_2010-01-01_2020-06-01_best5.tif")
bfast_best5_result <- raster("C:/bfast-test/NDMI_BFAST_best5.tif")
#plot(bfast_best5_result,zlim=c(min(bfast_all_result$native[],na.rm = TRUE),
#                               max(bfast_all_result$native[],na.rm = TRUE)))

#plot all results together
bfast_all_result_list <- list(
  #input = crop(bfast_native_input[[nlayers(bfast_native_input)]],bfast_best_result),
  native = crop(bfast_native_result,bfast_best_result),
  mean = disaggregate(crop(bfast_mean_result,bfast_best_result),3),
  central = disaggregate(crop(bfast_central_result,bfast_best_result),3),
  random = disaggregate(crop(bfast_random_result,bfast_best_result),3),
  best = disaggregate(bfast_best_result,3)
)
bfast_all_result_list_own <- list(
  central = bfast_central_result,
  random = bfast_random_result,
  mean = bfast_mean_result,
  best = bfast_best_result,
  native = bfast_native_result,
  best5 = bfast_best5_result
)
bfast_all_result <- stack(bfast_all_result_list)
detected_breaks_area <- lapply(bfast_all_result_list_own,
                          function(bfast_result){
                            100*sum(!is.na(bfast_result[]))/ncell(bfast_result)
                          })

png("fig_peru_deforestation.png",width = 480, height = 480)
plot(bfast_native_input[[nlayers(bfast_native_input)]])
dev.off()
png("fig_all_real_bfast.png",width = 960, height = 480)
plot(bfast_all_result,zlim=c(min(bfast_all_result$native[],na.rm = TRUE),
                             max(bfast_all_result$native[],na.rm = TRUE)))
dev.off()
click(bfast_all_result,n=Inf)
plot(bfast_all_result,zlim=c(2017,2020))
plot(bfast_all_result)

min(bfast_all_result$native[],na.rm = TRUE)
max(bfast_all_result$native[],na.rm = TRUE)
## check inputs
last <- nlayers(bfast_native_input)
plot(bfast_native_input[[last]],main="native last")
plot(bfast_central_downsampled[[last]],main="central last")
plot(bfast_random_downsampled[[last]],main="random last")
plot(bfast_mean_downsampled[[last]],main="mean last")
plot(bfast_best_downsampled_stack[[last]],main="best last")

a <- if (TRUE) 1 else 5
a
